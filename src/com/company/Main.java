package com.company;

import java.util.*;

public class Main {

    public Main() { }

    public static void main(String[] args) {
        /*try {
            Scanner scan = new Scanner(System.in);
            Data data = new Data(30, 6, 2020);
            System.out.println(data.toFormat(1,"."));
            System.out.println("Tydzień do przodu (1), Tydzień do tyłu (2)");
            String odpowiedz = scan.nextLine();
            int a = Integer.parseInt(odpowiedz);
            switch (a) {
                case 1 -> {
                    data.forward7();
                    System.out.println(data.dzienTygodnia());
                }
                case 2 -> {
                    data.backward7();
                    System.out.println(data.dzienTygodnia());
                }
            }

            System.out.println(data);
        } catch (Wyjatek var5) {
            System.out.println(var5.getMessage());
        }*/

        // Zadanie: 1 stycznia 2030 biezaca data o 100 tygodni wstecz
        Data a = new Data(1,1,2030); //podana data
        System.out.println(a.dzienTygodniav2()); // metoda z przesuwaniem tygodni, funkcje forward i backward
        System.out.println(a.dzienTygodnia()); // metoda z wyznaczaniem poprzez operacje modulo

        Data b = new Data(11,1,2020); //dzisiejsza data

        for(int i = 0;i<1000;i++){ // przesuniecie daty o 1000 tygodni
            b.backward7();
        }
        System.out.println(b.toFormat(3," ")); //wyswietlenie daty przesunietej

        List<Data> k1 = new ArrayList<>();
        Data k2[] = new Data[5];

        Data date1 = new Data(1,1,2018);
        Data date2 = new Data(12,7,2019);
        Data date3 = new Data(2,6,2019);
        Data date4 = new Data(25,4,2020);
        Data date5 = new Data(2,4,2018);

        k1.add(date1);
        k1.add(date2);
        k1.add(date3);
        k1.add(date4);
        k1.add(date5);

        k2[0] = date1;
        k2[1] = date2;
        k2[2] = date3;
        k2[3] = date4;
        k2[4] = date5;

        List listK = Arrays.asList(k2);

        System.out.println(" Pierwsza kolekcja nieposortowana");
        for(Data d: k1){
            System.out.println(d.toFormat(2,"/"));
        }

        System.out.println(" Druga kolekcja nieposortowana");
        for(Data d: k2){
            System.out.println(d.toFormat(3,"."));
        }

        Collections.sort(k1);
        Collections.sort(listK);

        System.out.println(" Pierwsza kolekcja po sortowaniu");
        for(Data d: k1){
            System.out.println(d.toFormat(2,"/"));
        }

        System.out.println(" Druga kolekcja po sortowaniu");
        for(Data d: k2){
            System.out.println(d.toFormat(3,"."));
        }
        System.out.println(Data.actualDate().toString());

        Data dataString = new Data("11","12","1999");
        System.out.println(dataString.toFormat(2,"/"));
        Data dataParse = Data.parse("17-10-2015");
        System.out.println(dataParse.toFormat(2,"/"));
    }
}