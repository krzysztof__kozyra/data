package com.company;

public class Miesiac {
    private int nrMiesiaca;
    private int ileDni;
    private String nazwaSlowna;

    public Miesiac(int nrMiesiaca, int ileDni, String nazwaSlowna) {
        this.nrMiesiaca = nrMiesiaca;
        this.ileDni = ileDni;
        this.nazwaSlowna = nazwaSlowna;
    }

    /**
     * Funkcja zwraca liczbe dni danego miesiaca przekazanego przez referencje
     * @return wartosc typu int okreslajaca liczbe dni danego miesiaca
     */
    public int getDni() {
        return this.ileDni;
    }

    /**
     * Funkcja zwraca numer porzadkowy miesiaca przekazanego przez referencje
     * @return wartosc typu int okreslajaca numer porzadkowy danego miesiaca
     */
    public int getnrMiesiaca() {
        return this.nrMiesiaca;
    }

    /**
     * Funkcja zwraca nazwe slowna miesiaca przekazanego przez referencje
     * @return lancuch znakowy typu String okreslajacy nazwe slowna danego miesiaca
     */
    public String getNazwaSlowna() {return this.nazwaSlowna;}
}
