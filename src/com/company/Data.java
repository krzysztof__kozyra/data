package com.company;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


public class Data implements Comparable<Data> {
    private int dzien;
    private int rok;
    private Miesiac miesiac;

    public Data(int dzien, int miesiac, int rok) {
        this.rok = rok;
        if(czyRokPrzestepny(this.rok)){
            this.miesiac = Miesiace.getMiesiacPrzestepny(miesiac);
            if (dzien >= 1 && dzien <= this.miesiac.getDni()) {
                this.dzien = dzien;
            } else {
                throw new Wyjatek("Błędny numer dnia: " + dzien);
            }
        }else{
            this.miesiac = Miesiace.getMiesiac(miesiac);
            if (dzien >= 1 && dzien <= this.miesiac.getDni()) {
                this.dzien = dzien;
            } else {
                throw new Wyjatek("Błędny numer dnia: " + dzien);
            }
        }
    }
    public Data (String dzien,String miesiac,String rok){
        this(Integer.parseInt(dzien),Integer.parseInt(miesiac),Integer.parseInt(rok));
    }

    /**
     * Zwraca obiekt klasy Data ktorego parametry dnia, miesiaca oraz roku, odpowiadaja aktualnej dacie
     * @return  obiekt klasy Data
     */
    public static Data actualDate(){
        Date date = new Date();
        LocalDate localDate =date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int[] currentdate = new int[3];
        currentdate[0] = localDate.getDayOfMonth();
        currentdate[1] = localDate.getMonthValue();
        currentdate[2] = localDate.getYear();

        Data data = new Data(currentdate[0],currentdate[1],currentdate[2]);

        return data;
    }

    /**
     * Sluzy do zwracania obiektu klasy Data, ktorego parametry otrzymywane sa z Stringa podanego jako argument funkcji
     * @param dataString lancuch znakowy typu String, opisujacy dzien, rok i miesiac
     * @return obiekt klasy Data
     */
    public static Data parse(String dataString){
        String[] dataArray = dataString.split("-");
        Data result = new Data(dataArray[0],dataArray[1],dataArray[2]);
        return result;
    }
    /**
     * Zwraca efekt porownania dwoch obiektow typu Data, uzywany do porownywania ktora data jest "starsza"
     * @param kal   obiekt typu Data, z ktorym porownujemy obiekt typu Data przekazany poprzez referencje this
     * @return  efekt porownania
     */
    public int compareTo(Data kal){
        int compareResult = Integer.compare(this.dzien, kal.dzien);

        if(compareResult == 0){
            compareResult = Integer.compare(this.miesiac.getDni(),kal.miesiac.getDni());
        }
        if(compareResult == 0){
            compareResult = Integer.compare(this.rok, kal.rok);
        }

        return compareResult;
    }

    /**
     * Funkcja w wersji z przesuwaniem tygodni(wykorzystujaca funkcje backward i forward) zwracajaca dzien tygodnia dla obiektu typu Data przekazywanego poprzez referencje this
     * @return  wartosc z tablicy String tydzien[]
     */
    public String dzienTygodniav2(){
        //23.11.2020 - poniedzialek
        Data sdata = new Data(23, 11, 2020);
        if(compareTo(sdata) == 1){
            while(compareTo(sdata) != -1){
                backward7v2(sdata);
            }
            int odl = Math.abs(sdata.dzien - this.dzien);
            return tydzien[odl];
        }else{
            while(compareTo(sdata) != 1){
                forward7v2(sdata);
            }
            if(sdata.miesiac.getnrMiesiaca() != this.miesiac.getnrMiesiaca()){
                backward7v2(sdata);
            }
            int odl = Math.abs(this.dzien % sdata.dzien);
            return tydzien[odl];
        }
    }
    public String tydzien[]={"poniedziałek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela"};
    public int liczbaDni[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    /**
     * Funkcja zwracajaca dzien tygodnia dla obiektu typu Data przekazywanego poprzez referencje this
     * @return  wartosc z tablicy String tydzien[]
     */
    public String dzienTygodnia(){
        int a,b,c;
        int wynik;
        int dzienRoku;

        dzienRoku = this.dzien + liczbaDni[this.miesiac.getnrMiesiaca() - 1];
        if((this.miesiac.getnrMiesiaca()>2) && (czyRokPrzestepny(this.rok))){
            dzienRoku++;
        }
        a=(this.rok-1) % 100;
        b=(this.rok-1) - a;
        c=a+(a/4);
        wynik=(((((b / 100) % 4) * 5) + c) % 7);
        wynik = wynik + dzienRoku - 1;
        wynik %= 7;
        return tydzien[wynik];
    }

    /**
     * Funkcja wykorzystywana do przesuwania o 7 dni do przodu, obiektu typu Data przekazanego przez referencje this
     */
    public void forward7() {
        if(czyRokPrzestepny(this.rok)){
            this.dzien += 7;
            if (this.miesiac.getnrMiesiaca() == 12 && this.miesiac.getDni() < this.dzien) {
                this.dzien -= this.miesiac.getDni();
                this.miesiac = Miesiace.getMiesiacPrzestepny(1);
                ++this.rok;
            }else if (this.miesiac.getDni() < this.dzien) {
                this.dzien -= this.miesiac.getDni();
                this.miesiac = Miesiace.getMiesiacPrzestepny(this.miesiac.getnrMiesiaca() + 1);
            }
        }else{
            this.dzien += 7;
            if (this.miesiac.getnrMiesiaca() == 12 && this.miesiac.getDni() < this.dzien) {
                this.dzien -= this.miesiac.getDni();
                this.miesiac = Miesiace.getMiesiac(1);
                ++this.rok;
            }else if (this.miesiac.getDni() < this.dzien) {
                this.dzien -= this.miesiac.getDni();
                this.miesiac = Miesiace.getMiesiac(this.miesiac.getnrMiesiaca() + 1);
            }
        }
    }

    /**
     * Funkcja wykorzystywana do przesuwania o tydzien do przodu obiektu typu Data, przekazanego jako argument
     * @param a obiekt typu Data
     */
    public static void forward7v2(Data a) {
        if(czyRokPrzestepny(a.rok)){
            a.dzien += 7;
            if (a.miesiac.getnrMiesiaca() == 12 && a.miesiac.getDni() < a.dzien) {
                a.dzien -= a.miesiac.getDni();
                a.miesiac = Miesiace.getMiesiacPrzestepny(1);
                ++a.rok;
            }else if (a.miesiac.getDni() < a.dzien) {
                a.dzien -= a.miesiac.getDni();
                a.miesiac = Miesiace.getMiesiacPrzestepny(a.miesiac.getnrMiesiaca() + 1);
            }
        }else{
            a.dzien += 7;
            if (a.miesiac.getnrMiesiaca() == 12 && a.miesiac.getDni() < a.dzien) {
                a.dzien -= a.miesiac.getDni();
                a.miesiac = Miesiace.getMiesiac(1);
                ++a.rok;
            }else if (a.miesiac.getDni() < a.dzien) {
                a.dzien -= a.miesiac.getDni();
                a.miesiac = Miesiace.getMiesiac(a.miesiac.getnrMiesiaca() + 1);
            }
        }
    }

    /**
     *  Funkcja wykorzystywana do przesuwania o 7 dni do tylu, obiektu typu Data przekazanego jako argument funkcji
     * @param a obiektu typu Data
     */
    public void backward7v2(Data a) {
        if(czyRokPrzestepny(a.rok)){
            a.dzien -= 7;
            if (a.miesiac.getnrMiesiaca() == 1 && a.dzien <= 0) {
                a.miesiac = Miesiace.getMiesiacPrzestepny(12);
                a.dzien = a.miesiac.getDni() - Math.abs(a.dzien);
                --a.rok;
            }
            if (a.dzien <= 0) {
                a.miesiac = Miesiace.getMiesiacPrzestepny(a.miesiac.getnrMiesiaca() - 1);
                a.dzien = a.miesiac.getDni() - Math.abs(a.dzien);
            }
        }else{
            a.dzien -= 7;
            if (a.miesiac.getnrMiesiaca() == 1 && a.dzien <= 0) {
                a.miesiac = Miesiace.getMiesiac(12);
                a.dzien = a.miesiac.getDni() - Math.abs(a.dzien);
                --a.rok;
            }
            if (a.dzien <= 0) {
                a.miesiac = Miesiace.getMiesiac(a.miesiac.getnrMiesiaca() - 1);
                a.dzien = a.miesiac.getDni() - Math.abs(a.dzien);
            }
        }
    }
    /**
     * Funkcja wykorzystywana do przesuwania o 7 dni do tylu, obiektu typu Data przekazanego przez referencje this
     */
    public void backward7() {
        if(czyRokPrzestepny(this.rok)){
            this.dzien -= 7;
            if (this.miesiac.getnrMiesiaca() == 1 && this.dzien <= 0) {
                this.miesiac = Miesiace.getMiesiacPrzestepny(12);
                this.dzien = this.miesiac.getDni() - Math.abs(this.dzien);
                --this.rok;
            }
            if (this.dzien <= 0) {
                this.miesiac = Miesiace.getMiesiacPrzestepny(this.miesiac.getnrMiesiaca() - 1);
                this.dzien = this.miesiac.getDni() - Math.abs(this.dzien);
            }
        }else{
            this.dzien -= 7;
            if (this.miesiac.getnrMiesiaca() == 1 && this.dzien <= 0) {
                this.miesiac = Miesiace.getMiesiac(12);
                this.dzien = this.miesiac.getDni() - Math.abs(this.dzien);
                --this.rok;
            }
            if (this.dzien <= 0) {
                this.miesiac = Miesiace.getMiesiac(this.miesiac.getnrMiesiaca() - 1);
                this.dzien = this.miesiac.getDni() - Math.abs(this.dzien);
            }
        }
    }

    /**
     * Funkcja zwraca informacje o tym czy rok jest przestepny
     * @param rok   rok dla ktorego chcemy sprawdzic przestepnosc, typu integer
     * @return  wartosc boolean, true gdy rok jest przestepny oraz false gdy nie jest przestepny
     */
    public static boolean czyRokPrzestepny(int rok) {
        if(rok % 4 == 0 && rok % 100 != 0 || rok % 400 == 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Funkcja wykorzystywana do wyswietlania obiektu typu Data jako typ String
     * @return  wartosc typu String oddzielona znakami "/" po dniu oraz miesiacu
     */
    public String toString() {

            return this.dzien + "/" + this.miesiac.getnrMiesiaca() + "/" + this.rok;
    }

    /**
     * Funkcja wykorzystywania do wyswietlania obiektu typu Data przekazanego przez referencje jako typ String, z zastosowaniem okreslonego formatowania
     * @param format    okresla typ formatowania wyswietlania daty,
     *                  <ol type="1">
     *                  <li>oddzielone separatorem parametry dnia, miesiaca oraz roku jako liczby</li>
     *                  <li>oddzielone separatorem parametry dnia tygodnia przedstawionego slownie oraz dnia, miesiaca oraz roku jako liczby</li>
     *                  <li>oddzielone separatorem parametry dnia tygodnia przedstawionego slownie oraz dnia, roku jako liczby i miesiaca przedstawionego slownie</li>
     *                  <li>oddzielone separatorem parametry dnia tygodnia przedstawionego slownie oraz dnia, roku i miesiaca przedstawionego za pomoca cyfr rzymskich</li>
     *                  </ol>
     * @param separator ciag znakowy typu String ktory w odpowiedni sposob oddziela parametry wyswietlane przez metode
     * @return  lancuch znakowy typu String zawierajacy parametry obiektu typu Data dpowiednio sformatowanego, lub blad jesli ustawiono bledny typ formatu
     */
    public String toFormat(int format,String separator){
        switch(format){
            case 1:
                return this.dzien + separator + this.miesiac.getnrMiesiaca() + separator + this.rok;
            case 2:
                return this.dzienTygodnia() + "," + this.dzien + separator + this.miesiac.getnrMiesiaca() + separator + this.rok;
            case 3:
                return this.dzienTygodnia() + "," + this.dzien + separator + this.miesiac.getNazwaSlowna() + separator + this.rok;
            case 4:
                return this.dzienTygodnia() + "," + IntegerToRomanNumeral(this.dzien) + separator + IntegerToRomanNumeral(this.miesiac.getnrMiesiaca()) + separator + IntegerToRomanNumeral(this.rok);
            default:
                return "Błędny typ formatu";
        }
    }

    /**
     * Funckja zamieniajaca liczby arabskie na rzymskie
     * @param x liczba arabska ktora ma zostac zamieniona
     * @return  lancuch znakowy ktory przedstawia liczbe w zapisie rzymskim odpowiadajaca liczbe podana jako argument
     */
    public static String IntegerToRomanNumeral(int x) {
        if (x < 1 || x > 3999)
            return "wartosc bledna";
        StringBuilder s = new StringBuilder();
        while (x >= 1000) {
            s.append("M");
            x -= 1000;        }
        while (x >= 900) {
            s.append("CM");
            x -= 900;
        }
        while (x >= 500) {
            s.append("D");
            x -= 500;
        }
        while (x >= 400) {
            s.append("CD");
            x -= 400;
        }
        while (x >= 100) {
            s.append("C");
            x -= 100;
        }
        while (x >= 90) {
            s.append("XC");
            x -= 90;
        }
        while (x >= 50) {
            s.append("L");
            x -= 50;
        }
        while (x >= 40) {
            s.append("XL");
            x -= 40;
        }
        while (x >= 10) {
            s.append("X");
            x -= 10;
        }
        while (x >= 9) {
            s.append("IX");
            x -= 9;
        }
        while (x >= 5) {
            s.append("V");
            x -= 5;
        }
        while (x >= 4) {
            s.append("IV");
            x -= 4;
        }
        while (x >= 1) {
            s.append("I");
            x -= 1;
        }
        return s.toString();
    }
}