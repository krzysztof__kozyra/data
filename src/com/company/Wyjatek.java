package com.company;

public class Wyjatek extends RuntimeException {
    public Wyjatek() {
    }

    public Wyjatek(String message) {
        super(message);
    }

    public Wyjatek(Throwable cause) {
        super(cause);
    }

    public Wyjatek(String message, Throwable cause) {
        super(message, cause);
    }
}
