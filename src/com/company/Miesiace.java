package com.company;

public class Miesiace {
    private static final Miesiac[] miesiacePrzestepne = new Miesiac[]{new Miesiac(1, 31, "Styczeń"), new Miesiac(2, 29, "Luty"), new Miesiac(3, 31, "Marzec"), new Miesiac(4, 30, "Kwiecień"), new Miesiac(5, 31, "Maj"), new Miesiac(6, 30, "Czerwiec"), new Miesiac(7, 31, "Lipiec"), new Miesiac(8, 31, "Sierpień"), new Miesiac(9, 30, "Wrzesień"), new Miesiac(10, 31, "Październik"), new Miesiac(11, 30, "Listopad"), new Miesiac(12, 31, "Grudzień")};
    private static final Miesiac[] miesiace = new Miesiac[]{new Miesiac(1, 31, "Styczeń"), new Miesiac(2, 28, "Luty"), new Miesiac(3, 31, "Marzec"), new Miesiac(4, 30, "Kwiecień"), new Miesiac(5, 31, "Maj"), new Miesiac(6, 30, "Czerwiec"), new Miesiac(7, 31, "Lipiec"), new Miesiac(8, 31, "Sierpień"), new Miesiac(9, 30, "Wrzesień"), new Miesiac(10, 31, "Październik"), new Miesiac(11, 30, "Listopad"), new Miesiac(12, 31, "Grudzień")};
    public Miesiace() {
    }


    /**
     * Funkcja uzywana do tworzenia obiektu typu Data, parametru miesiac
     * @param nrMiesiaca    numer porzadkowy miesiaca ktory chcemy stworzyc
     * @return  obiekt typu Miesiac
     */
    public static Miesiac getMiesiac(int nrMiesiaca) {
        try {
            return nrMiesiaca <= 1 && nrMiesiaca >= 12 ? null : miesiace[nrMiesiaca - 1];
        } catch (ArrayIndexOutOfBoundsException var2) {
            throw new Wyjatek("Podałeś błędny numer miesiąca: " + nrMiesiaca);
        }
    }
    /**
     * Funkcja uzywana do tworzenia obiektu typu Data, parametru miesiac, stosowana w przypadku wystapienia roku przestepnego
     * @param nrMiesiaca    numer porzadkowy miesiaca ktory chcemy stworzyc
     * @return  obiekt typu Miesiac
     */
    public static Miesiac getMiesiacPrzestepny(int nrMiesiaca) {
        try {
            return nrMiesiaca <= 1 && nrMiesiaca >= 12 ? null : miesiacePrzestepne[nrMiesiaca - 1];
        } catch (ArrayIndexOutOfBoundsException var2) {
            throw new Wyjatek("Podales bledny numer miesiaca: " + nrMiesiaca);
        }
    }
}
